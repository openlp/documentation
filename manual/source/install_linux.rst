Installing OpenLP on Linux
==========================

Setting up OpenLP varies by distribution. We have included documentation on popular distributions or distributions that
have OpenLP available from their package managers. It should be possible to get OpenLP running on nearly any Linux
distribution.

Ubuntu
----------------

Installation of OpenLP on Ubuntu can either be done through the Software Center or the command line. Command line
instructions will be included at the end of this section on Ubuntu and its variants.

OpenLP supports Ubuntu 23.04 or later.

Ubuntu, Xubuntu
^^^^^^^^^^^^^^^

To install OpenLP in Ubuntu, open the Software Center, search for OpenLP and click :guilabel:`Install`.

.. image:: pics/4searchopenlp.png

You will see the install progress as OpenLP and the dependencies required for
it to run are downloaded.

.. image:: pics/5installprogressubuntu.png

After installation you should see that OpenLP is installed.

.. image:: pics/6installcompleteubuntu.png

Kubuntu
^^^^^^^

Open up Discover, type "openlp" in the search box and press :kbd:`Enter`. Next, click on the :guilabel:`Install` button.


Ubuntu Command Line Install
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Open a terminal and type the following command to install OpenLP::

  user@ubuntu:~$ sudo apt-get install openlp

OpenLP should now be available in your desktop's menu system, but if you wish 
to run OpenLP from the command line type::

  user@ubuntu:~$ openlp

Fedora
------

To install OpenLP on Fedora follow the instructions below. The instructions
describes how to do the install when using GNOME, KDE or the command line.

**Note** When installing OpenLP you will need to have administrator privileges.
You will be asked for the administrator password to install.

Fedora (GNOME)
^^^^^^^^^^^^^^

Installing with Fedora you will use the default Add/Remove Programs available
from :menuselection:`System --> Administration --> Add/Remove Programs` in 
Fedora 14 and below or in :menuselection:`Applications --> System Tools --> 
Add/Remove Programs`

.. image:: pics/1fedoraaddremove.png

Search for OpenLP in the search box

.. image:: pics/2fedoraaddremove.png

Check the check box for OpenLP then click :guilabel:`Apply`

.. image:: pics/3fedoraaddremove.png

Click :guilabel:`Continue` to confirm installing any additional software.

.. image:: pics/4fedoraadditionalconfirm.png

You should now see the packages downloading in the lower left corner.

.. image:: pics/5fedoraaddremove.png

Click :guilabel:`Run` to run OpenLP now, or :guilabel:`Close` to run OpenLP
later.

.. image:: pics/6fedoracomplete.png

OpenLP will be available in :menuselection:`Applications --> Sound & Video --> OpenLP`

Fedora (KDE)
^^^^^^^^^^^^

From the Kickoff open *Software Management*. Type OpenLP into the search
box. Then click :guilabel:`Find by name` or press :kbd:`Enter`.

.. image:: pics/1fedoragetremove.png 

Select OpenLP from the search results. Next, click :guilabel:`Apply`

.. image:: pics/2fedoragetremove.png

Now give permission to install other software dependencies that are needed by
clicking :guilabel:`Continue`

.. image:: pics/3fedoraadditionalchanges.png

Once completed you can run OpenLP by clicking on its logo in the completed 
install notification, or from the Kickoff menu.

.. image:: pics/4fedoracompleted.png

Fedora Command Line Install
^^^^^^^^^^^^^^^^^^^^^^^^^^^

To install OpenLP from the command line type::

  user@fedora:~$ sudo yum install openlp

OpenLP should now be available in your desktop's menu system, but if you wish 
to run OpenLP from the command line type::

  user@fedora:~$ openlp

Linux Mint
----------

The following instructions are for a standard Linux Mint install. These 
instructions will not work with Linux Mint Debian Edition. Most users should
be able to follow the instructions for :ref:`mint-softman`, but if your are
running Linux Mint without the standard desktop please see the
:ref:`mint-command` install instructions.

.. _mint-softman:

Software Manager Install
^^^^^^^^^^^^^^^^^^^^^^^^

The administrator password will be required several times during the 
installation. To install OpenLP open the Software Manager from the Mint Menu.

.. image:: pics/mint1.png

Type OpenLP into the search box to locate the OpenLP package.

.. image:: pics/mint7.png

Click on OpenLP from the results then click :guilabel:`Install`.

.. image:: pics/mint8.png

After OpenLP is installed you can close the Software Manager and start OpenLP
from the *Sound and Video* section of the Mint Menu.

.. _mint-command:

Linux Mint Command Line Install
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Open a terminal and type in the following command to install OpenLP::

  user@mint:~$ sudo apt-get install openlp

OpenLP should now be available in your desktop's menu system, but if you wish 
to run OpenLP from the command line type::

  user@mint:~$ openlp

openSUSE
--------

OpenLP is available using 1-Click Install on the openSUSE Build Service
website. Go to the `openSUSE Build Service site <http://software.opensuse.org>`_
and type :kbd:`openlp` into the search box.

.. image:: pics/suse1.png

Then click :guilabel:`1-Click Install`

.. image:: pics/suse2.png

Select :guilabel:`Open with YaST 1-Click Install` then click :guilabel:`OK`

.. image:: pics/suse3.png

Additional repositories may need to be enabled. This will be performed
automatically. Accept the default options by clicking :guilabel:`Next`

.. image:: pics/suse5.png

Confirm you want to install OpenLP and click :guilabel:`Next`

.. image:: pics/suse6.png

Before the installation takes place confirm adding the repositories and
installing the software again. Click :guilabel:`Next` to continue.

.. image:: pics/suse7.png

Since this is a third party application the software key will have to be 
accepted. Click :guilabel:`Trust` to accept the software key.

.. image:: pics/suse8.png

OpenLP will now be installed. When the install process is completed click
:guilabel:`Finish`.

.. image:: pics/suse9.png

OpenLP will now be available for use.

Arch Linux
----------

OpenLP is available on Arch Linux through the Arch User Repository, or AUR. Info
on the AUR can be found `at the Arch wiki <https://wiki.archlinux.org/index.php/Arch_User_Repository>`_.

There are a variety of tools and methods to install from the AUR. For this
example we will demonstrate using Yaourt. For more info on Yaourt please see 
the `Yaourt documentation <https://wiki.archlinux.org/index.php/Yaourt>`_.  

From a terminal type::

  user@arch:~$ yaourt -S openlp

OpenLP should now be available in your desktop's menu system, but if you wish
to run OpenLP from the command line type::

  user@arch:~$ openlp
